CONTENTS OF THIS FILE:-
-----------------------

  * Overview
  * Features
  * Requirements
  * Future enhancements




Overview:-
----------

Comment attachment module enables comments to have files attached to it. 
Suppose you have a forum and your forum members submits a post/comment and
he/she want to upload a PDF or an image file attched to his post/comment.
The forum module which comes with the drupal core does not provide any option
to attach an image or any file to a post/comment. This module solves this 
simple problem by allowing an UI for uploading files as an attachment to a
comment and also lists the files attached to a particular comment.


Features:-
----------
* Comments to a node are enhanced with file attachment feature
* An administration/config page which allows the site-admin to enable/disable
  the visibility of attachment to comments


Requirements:-
--------------
This module is for drupal 7.


Future enhancements:-
---------------------
* An image icon will be there before each filename in the attached file list
  indicating the MIME type of the file attached.
* A thumbnail will comeup on hovering the file-name if it is a image file.
