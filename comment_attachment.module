<?php
/**
 * @file
 * Enables comments to have files attached to them.
 */

/**
 * Displays help and module information, Implements hook_help().
 *
 * @param string $path 
 *   Which path of the site we're using to display help
 * @param mixed $arg 
 *   Array that holds the current path as returned from arg() function
 */
function comment_attachment_help($path, $arg) {
  switch ($path) {
    case "admin/help#comment_attachment":
      return '<p>' . t("Enables attaching a file for a comment for a forum topic") . '</p>';

    break;

    default:
      // No default action.
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function comment_attachment_form_comment_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id != 'comment_node_forum_form') {
    return NULL;
  }
  if (isset($form_state['comment']->cid)) {
    $header = array('file_name' => t('Attached files (select to remove)'));
    $options = array();
    $attachments = $form_state['comment']->attachments;
    foreach ($attachments as $attachment) {
      $text = $attachment->filename;
      $href = file_create_url($attachment->uri);
      $options[$attachment->fid] = array('file_name' => l($text, $href));
    }

    $form['attachment_table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No attachment found'),
    );
  }

  $comment_attachment_upload_location = variable_get('comment_attachment_upload_location', 'forum_comment_attchments');
  $form['attachment_upload'] = array(
    '#type' => 'managed_file',
    '#size' => 22,
    '#title' => t('Attachment'),
    '#description' => t('Upload an attachment file for this comment.'),
    '#upload_location' => 'public://' . $comment_attachment_upload_location . '/',
  );
}

/**
 * Updates the attachment list on inserting and updating a comment.
 * 
 * @param mixed $comment
 *   drupal comment object
 * 
 * @author Bikash Barman <bikash@grmtech.com>
 */
function comment_attachment_manage_file($comment) {
  if (isset($comment->attachment_upload) && $comment->attachment_upload) {
    comment_attachment_save_file($comment->attachment_upload, $comment->cid);
  }

  if (isset($comment->attachment_table)) {
    $attachment_table = $comment->attachment_table;
    if (array_sum($attachment_table)) {
      $array_values = array_values($attachment_table);
      foreach ($array_values as $array_value) {
        $file_id = (int) $array_value;
        if ($file_id) {
          comment_attachment_delete_file($file_id, $comment->cid);
        }
      }
    }
  }
}

/**
 * Implements hook_comment_insert().
 */
function comment_attachment_comment_insert($comment) {
  comment_attachment_manage_file($comment);
}

/**
 * Implements hook_comment_update().
 */
function comment_attachment_comment_update($comment) {
  comment_attachment_manage_file($comment);
}

/**
 * Implements hook_comment_delete().
 */
function comment_attachment_comment_delete($comment) {
  foreach ($comment->attachments as $attachment) {
    comment_attachment_delete_file($attachment->fid, $comment->cid);
  }
}

/**
 * Deletes an attachment file.
 */
function comment_attachment_delete_file($file_id, $cid) {
  $file = file_load($file_id);
  file_usage_delete($file, 'comment_attachment', 'attachment', $cid);
  file_delete($file);
}

/**
 * Saves an attachment file.
 */
function comment_attachment_save_file($file_id, $cid) {
  $file = file_load($file_id);
  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);
  file_usage_add($file, 'comment_attachment', 'attachment', $cid);
}

/**
 * Implements hook_comment_load().
 */
function comment_attachment_comment_load($comments) {
  foreach ($comments as $cid => $comment) {
    $comment->attachments = comment_attachment_get_attachments($cid);
  }
}

/**
 * Returns the attachment file list for a comment.
 */
function comment_attachment_get_attachments($cid) {
  $attachments = array();
  $query = db_select('file_usage', 'f')
  ->fields('f', array('fid'))
  ->condition('module', 'comment_attachment', '=')
  ->condition('type', 'attachment', '=')
  ->condition('id', $cid, '=')
  ->execute();
  while ($record = $query->fetchObject()) {
    $file = file_load($record->fid);
    $attachments[] = $file;
  }
  return $attachments;
}

/**
 * Implements hook_theme().
 */
function comment_attachment_theme() {
  return array(
    'file_attachment' => array(
      'template' => 'file-attachment',
      'variables' => array('attachments' => NULL),
    ),
  );
}

/**
 * Implements hook_comment_view_alter().
 */
function comment_attachment_comment_view_alter(&$build) {
  $comment_attachment_visibility = variable_get('comment_attachment_visibility', 'show');
  if ($comment_attachment_visibility == 'show' || $comment_attachment_visibility == 'show_with_image') {
    $show = TRUE;
  }
  else {
    $show = FALSE;
  }

  if ($show && isset($build['comment_body']['#object']->attachments) && $build['comment_body']['#object']->attachments) {
    $attached_items = array();
    foreach ($build['comment_body']['#object']->attachments as $attachment) {
      if ($comment_attachment_visibility == 'show_with_image') {
        $attached_items[] = theme('file_attachment', array('attachment' => $attachment));
      }
      else {
        $text = $attachment->filename;
        $href = file_create_url($attachment->uri);
        $attached_items[] = l($text, $href);
      }
    }

    $default_comment_attachment_label = 'Attachments';
    $comment_attachment_label = variable_get('comment_attachment_label', $default_comment_attachment_label);
    if (empty($comment_attachment_label)) {
      $comment_attachment_label = $default_comment_attachment_label;
    }
    $build['attachments'] = array(
      '#items' => $attached_items,
      '#theme' => 'item_list',
      '#prefix' => '<div><h3>' . $comment_attachment_label . ':-</h3>',
      '#suffix' => '</div>',
    );
  }
}

/**
 * Implements hook_menu().
 * 
 * @author Bikash Barman <bikash@grmtech.com>
 */
function comment_attachment_menu() {
  $items = array();
  $path = drupal_get_path('module', 'comment_attachment');
  $items['admin/config/media/comment-attachment'] = array(
    'title' => 'Comment attachments settings',
    'description' => 'Set your preferences for comment attachment module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('comment_attachment_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'comment_attachment.admin.inc',
    'file path' => $path,
  );
  return $items;
}
