<?php

/**
 * @file
 * Admin page callbacks for the comment_attachment module.
 */

/**
 * Generates form array for comment attachment settings.
 * 
 * @author Bikash Barman <bikash@grmtech.com>
 */
function comment_attachment_settings($form, &$form_state) {
  $options = array(
    'show' => t('Show'),
    'show_with_image' => t('Show with image'),
    'hide' => t('Hide'),
  );
  $form['attachment_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Attachment visibility'),
    '#default_value' => variable_get('comment_attachment_visibility', 'show'),
    '#options' => $options,
    '#description' => t('Display the attached files just below each comment.'),
  );

  $form['attachment_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Attachment label'),
    '#default_value' => variable_get('comment_attachment_label', 'Attachments'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form['attachment_upload_location'] = array(
    '#type' => 'textfield',
    '#description' => t('A local file system path where comment attachments will be stored. This directory must be writable by Drupal. If you rename this directory only newly uploaded files will be uploaded to the new directory. Old uploads will not be moved.<br />Alphabet, digit, hyphen, underscore, dot, forward-slash and brackets are allowed only "a-z A-Z 0-9 . - _ / () {} [] "'),
    '#field_prefix' => 'public://',
    '#field_suffix' => '/',
    '#title' => t('Attachment upload location'),
    '#default_value' => variable_get('comment_attachment_upload_location', 'forum_comment_attchments'),
    '#size' => 51,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $form = system_settings_form($form);
  $form['#submit'][] = 'comment_attachment_settings_submit';
  return $form;
}

/**
 * Form validation for 'comment_attachment_settings'.
 */
function comment_attachment_settings_validate($form, &$form_state) {
  $location = $form_state['values']['attachment_upload_location'];
  $match = array();
  if (preg_match_all('#([^a-z0-9\-_./()\[\]{}]+)#i', $location, $match)) {
    $invalid_char = $match[0][0];
    form_set_error('attachment_upload_location', t('Invalid charecter "@invalid_char" for directory name.', array('@invalid_char' => $invalid_char)));
  }
}

/**
 * Form submit action handler for 'comment_attachment_settings'.
 */
function comment_attachment_settings_submit($form, &$form_state) {
  variable_set('comment_attachment_upload_location', trim($form_state['values']['attachment_upload_location'], '/'));
  variable_set('comment_attachment_visibility', $form_state['values']['attachment_visibility']);
  variable_set('comment_attachment_label', $form_state['values']['attachment_label']);
}
