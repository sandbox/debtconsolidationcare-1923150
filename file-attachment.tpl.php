<?php

/**
 * @file
 * Displays all the attachments to a comment.
 *
 * The submission string indicates when and by whom a topic was submitted.
 *
 * Available variables:
 * - $attachments: The attachments to a comment, this can be found in
 *   $comment->attachments.
 */
?>

<?php if (isset($attachment) && $attachment): ?>
    <?php
      $text = $attachment->filename;
      $href = file_create_url($attachment->uri);
    ?>
    <div>
      <?php if (preg_match('#^image/#i', $attachment->filemime)): ?>
        <?php
          $img_prop = array(
            'style_name' => 'medium',
            'path' => $attachment->uri,
            'alt' => $text,
          );
          print theme('image_style', $img_prop);
        ?><br/>
      <?php endif; ?>
      <?php print l($text, $href); ?>
    </div>
<?php endif; ?>
